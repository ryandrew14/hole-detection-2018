import solution
import unittest
from solution import Types, Compositions


class TestSolution(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        solution.DISPLAY = True

    def test_simple_1(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.simple_1)
        self.assertEqual(4, len(hole_features))
        self.assertEqual(2, len(compound_hole_features))
        self.assertEqual(4, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        #self.assertEqual(2, len([h for h in hole_features if h['composition'] == Compositions.TAPERED]))
        #self.assertEqual(2, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))
        #self.assertEqual(2, len([h for h in compound_hole_features if h['type'] == Types.THROUGH]))
        #self.assertEqual(2, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERSINK]))

'''
    def test_simple_2(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.simple_2)
        self.assertEqual(2, len(hole_features))
        self.assertEqual(1, len(compound_hole_features))
        self.assertEqual(1, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(1, len([h for h in hole_features if h['type'] == Types.BLIND]))
        self.assertEqual(1, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))
        self.assertEqual(1, len([h for h in compound_hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(1, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERBORE]))

    def test_simple_3(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.simple_3)
        self.assertEqual(50, len(hole_features))
        self.assertEqual(25, len(compound_hole_features))
        self.assertEqual(38, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(12, len([h for h in hole_features if h['type'] == Types.BLIND]))
        self.assertEqual(37, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))
        self.assertEqual(13, len([h for h in hole_features if h['composition'] == Compositions.TAPERED]))
        self.assertEqual(25, len([h for h in compound_hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(12, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERBORE]))
        self.assertEqual(13, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERSINK]))

    def test_intermediate_1(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.intermediate_1)
        self.assertEqual(6, len(hole_features))
        self.assertEqual(0, len(compound_hole_features))
        self.assertEqual(2, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(4, len([h for h in hole_features if h['type'] == Types.BLIND]))
        self.assertEqual(0, len([h for h in hole_features if h['composition'] == Compositions.TAPERED]))
        self.assertEqual(6, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))

    def test_intermediate_2(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.intermediate_2)
        self.assertEqual(10, len(hole_features))
        self.assertEqual(2, len(compound_hole_features))
        self.assertEqual(10, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(8, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))
        self.assertEqual(2, len([h for h in hole_features if h['composition'] == Compositions.TAPERED]))
        self.assertEqual(2, len([h for h in compound_hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(2, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERSINK]))

    def test_intermediate_3(self):
        # THIS IS A TOUGH ONE
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.intermediate_3)
        self.assertEqual(48, len(hole_features))
        self.assertEqual(18, len(compound_hole_features))
        self.assertEqual(14, len([h for h in hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(34, len([h for h in hole_features if h['type'] == Types.BLIND]))
        self.assertEqual(32, len([h for h in hole_features if h['composition'] == Compositions.BASIC]))
        self.assertEqual(16, len([h for h in hole_features if h['composition'] == Compositions.TAPERED]))
        self.assertEqual(10, len([h for h in compound_hole_features if h['type'] == Types.THROUGH]))
        self.assertEqual(8, len([h for h in compound_hole_features if h['type'] == Types.BLIND]))
        self.assertEqual(10, len([h for h in compound_hole_features if h['composition'] == Compositions.COUNTERBORE]))
        self.assertEqual(8, len([h for h in compound_hole_features if h['composition'] == Compositions.COMPOUND]))

    def test_challenge_1(self):
        hole_features, compound_hole_features = solution.interrogate(solution.TestFiles.challenge_1)
        # we will leave this one up to you to decide how many there are!
'''

def run_tests():
    suite = unittest.TestLoader().loadTestsFromTestCase(TestSolution)
    print unittest.TextTestRunner(verbosity=4).run(suite)

