# Paperless Parts Coding Challenge Solution

Ryan Drew - Northeastern University

### Completion of Objectives
I unfortunately was only able to complete the first objective, locating hole features. Some reasons for this could be:
- Limited time. I only alotted myself four hours to work due to heavy workload in other classes as well as midterms.
- Misuse of time. I spent a lot of time playing around in the environment trying to figure out how to get
the data I would need to figure out if a hole is blind or not, but could not find it in the alotted time. I believe the compound
hole problem would have been easier to solve, using the common() function.
- Mistakes early on. I spent a long time not knowing about Surfaces (which made it incredibly easy to discern
cylinders and cones from flat surfaces), and instead I tried to use curveOnSurface() to do this, which was much harder.

### Future Objectives
The first few things I would do given more time are to go and solve the compound holes problem, as I think I could do it fairly quick.
In addition, the depth should be pretty easy to find, after exploring more details of the FreeCAD Python library. Using these, I could find
hole types. Lastly, I would abstract what I do have once I see what the final code will look like to clean things up.
