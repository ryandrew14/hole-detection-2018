import FreeCAD
import FreeCADGui
import Part
import os

LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
DISPLAY = True


class TestFiles:
    """Convenience class to grab test file paths"""
    simple_1 = os.path.join(LOCAL_PATH, 'parts', 'simple_1.step')

    simple_2 = os.path.join(LOCAL_PATH, 'parts', 'simple_2.step')

    simple_3 = os.path.join(LOCAL_PATH, 'parts', 'simple_3.step')

    intermediate_1 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_1.step')

    intermediate_2 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_2.step')

    intermediate_3 = os.path.join(LOCAL_PATH, 'parts', 'intermediate_3.step')

    challenge_1 = os.path.join(LOCAL_PATH, 'parts', 'challenge_1.step')


class Types:
    THROUGH = 'through'
    BLIND = 'blind'


class Compositions:
    BASIC = 'basic'
    TAPERED = 'tapered'
    COUNTERSINK = 'countersink'
    COUNTERBORE = 'counterbore'
    COMPOUND = 'compound'


def display(obj, name, color=(0.5, 0.5, 0.5), transparency=0, visibility=True):
    """
    Utility function to display a face or group of faces
    
    :param obj: Either a single ``Part.Face``, a list or set of ``Part.Face`` 
    objects, or a ``Part.Shape`` object
    :param name: ``string`` Name to give the face or group of faces
    :param color: Three-tuple of RGB color values for the face(s) display 
    color. Must be floats so make sure the numbers in the tuple have a decimal 
    point at the end as seen in the inputs. Default color is gray
    :param transparency: Number between 0 and 1, determines how see through 
    the part is, default to 0
    :param visibility: ``Boolean`` sets default visibility of object, can be 
    toggled with space bar in FreeCAD
    """
    # check to see if a document has been created, if not, make one
    if FreeCADGui.ActiveDocument is None:
        FreeCAD.newDocument()

    if isinstance(obj, Part.Face):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    elif isinstance(obj, list) or isinstance(obj, set):
        sh = Part.makeShell(list(obj))
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = sh
    elif isinstance(obj, Part.Shape):
        FreeCAD.ActiveDocument.addObject('Part::Feature', name).Shape = obj
    else:
        print('Incompatible display type!')
        return

    # Now set the color and transparency.
    # After creating the object, it is always set to the Gui active object;
    # object can also be referenced with the syntax:
    # FreeCADGui.ActiveDocument.getObject(name)
    FreeCADGui.ActiveDocument.ActiveObject.DiffuseColor = color
    FreeCADGui.ActiveDocument.ActiveObject.Transparency = transparency
    FreeCADGui.ActiveDocument.ActiveObject.Visibility = visibility


def interrogate(path_to_file):
    """
    Implementation of hole detection logic. Put your solution here! You can
    remove the example logic below.
    
    :param path_to_file: ``string`` path to the part file, use ``TestFiles`` 
    for convenience
    :return: List of dictionaries of hole features and compound hole features
    """
    # code to read the part into shape
    shape = Part.Shape()
    shape.read(path_to_file)

    # refer to this face list for indices
    face_list = list(shape.Faces)

    cylinders = []
    cones = []

    # find all cones and cylinders in a part
    for face in face_list:
        if isinstance(face.Surface, Part.Cylinder):
            cylinders.append(face)
        elif isinstance(face.Surface, Part.Cone):
            cones.append(face)

    # copy list for easy checking
    cyl_copy = list(cylinders)
    cylinder_features = []

    for cyl in cylinders:
        # initialize data entry
        dictEntry = {}
        # combine faces into hole features
        for otherCyl in cyl_copy:
            if sameCylinder(cyl, otherCyl):
                if 'faces' not in dictEntry.keys():
                    dictEntry['faces'] = [cyl, otherCyl]
                else:
                    dictEntry['faces'].append(otherCyl)
        # if a hole feature was found, add other values (incomplete)
        if dictEntry.keys() != []:
            dictEntry['type'] = getHoleType(dictEntry['faces'])
            #dictEntry['composition'] = getComposition(dictEntry['faces'])
            #dictEntry['depth'] = getDepth(cyl)
            dictEntry['diameter'] = cyl.Surface.Radius * 2
            cyl_copy.remove(cyl)
            cylinder_features.append(dictEntry) 

    
    # repeat finding, but for cones
    # NOTE: I would totally abstract this later on in the solution, but for now I don't know
    # which functions will be different from one block of code to the other, so I didn't want
    # to abstract yet
    cone_copy = list(cones)
    cone_features = []

    for cone in cones:
        dictEntry = {}
        for otherCone in cone_copy:
            if sameCone(cone, otherCone):
                if 'faces' not in dictEntry.keys():
                    dictEntry['faces'] = [cone, otherCone]
                else:
                    dictEntry['faces'].append(otherCone)
        if dictEntry.keys() != []:
            cone_copy.remove(cone)
            cone_features.append(dictEntry)


    # you should output a list of hole features

    # output list of cylinder and cone features
    hole_features = cylinder_features + cone_features

    # each hole feature should be described by a dictionary with any information
    # you're able to identify; here is a sample dict for a hole feature
    '''
    sample_hole_feature1 = {
        'faces': [face_list[0], face_list[1]],
        'type': Types.BLIND,
        'composition': Compositions.BASIC,
        'depth': 10.0,
        'diameter': 10.0,
    }
    sample_hole_feature2 = {
        'faces': [face_list[2], face_list[3]],
        'type': Types.THROUGH,
        'composition': Compositions.BASIC,
        'depth': 10.0,
        'diameter': 5.0,
    }
    hole_features.append(sample_hole_feature1)
    hole_features.append(sample_hole_feature2)
    '''

    # also output a list of compound hole features
    compound_hole_features = []

    # compound hole features should be represented as a list of hole features with
    # additional information populated to them
    # lets assume the two sample hole features above have the same central axis
    '''
    sample_compound_hole_feature = {
        'sub_features': [sample_hole_feature1, sample_hole_feature2],
        'type': Types.THROUGH,
        'composition': Compositions.COUNTERBORE,
        'depth': sample_hole_feature1['depth'] + sample_hole_feature2['depth'],
        'diameter': min(sample_hole_feature1['diameter'], sample_hole_feature2['diameter'])
    }
    compound_hole_features.append(sample_compound_hole_feature)
    '''

    # example of how to use the display function
    # only display if DISPLAY is True
    if DISPLAY:
        example_list = [f for f in shape.Faces
                        if isinstance(f.Surface, Part.Cylinder)]
        display(shape, 'SolidBody', visibility=True)
        display(example_list, 'SampleGroupingOfFaces', color=(0., 0.5, 0.))
        display(shape.Faces[0], 'SampleSingleFace', color=(0., 0., 0.5),
                visibility=False)

    # this fits the display to the items that you just displayed
    FreeCADGui.SendMsgToActiveView('ViewFit')

    # now return the output dictionary
    return hole_features, compound_hole_features

# Are 2 faces parts of the same cylinder, but not the same face?
# Face Face -> Boolean
def sameCylinder(c1, c2):
    return (not c1 == c2) and c1.Surface.Axis == c2.Surface.Axis and c1.Surface.Center == c2.Surface.Center and c1.Surface.Radius == c2.Surface.Radius

# Are 2 faces parts of the same Cone, but not the same face?
# Face Face -> Boolean
def sameCone(c1, c2):
    return (not c1 == c2) and c1.Surface.Axis == c2.Surface.Axis and c1.Surface.Center == c2.Surface.Center and c1.Surface.Radius == c2.Surface.Radius

# Figure out the hole type given a hole feature's list of faces
# List[Face] -> Types
def getHoleType(faces):
    if isBlind(faces):
        return Types.BLIND
    else:
        return Types.THROUGH

# Is this hole blind?
# List[Face] -> Boolean
def isBlind(faces):
    return False    

# Figure out the hole composition given a hole feature's list of faces
# List[Face] -> Number
#def getComposition(faces):

# Get the depth of a face
# Face -> Number
#def getDepth(face):

